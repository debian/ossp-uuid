   _        ___  ____ ____  ____                _     _
  |_|_ _   / _ \/ ___/ ___||  _ \   _   _ _   _(_) __| |
  _|_||_| | | | \___ \___ \| |_) | | | | | | | | |/ _` |
 |_||_|_| | |_| |___) |__) |  __/  | |_| | |_| | | (_| |
  |_|_|_|  \___/|____/____/|_|      \__,_|\__,_|_|\__,_|

  OSSP uuid - Universally Unique Identifier
  Version 1.6.4 (2024-12-09)

  ABSTRACT

  OSSP uuid is a C API and corresponding CLI program for the generation
  and analysis of DCE 1.1 and, IETF RFC-9562- compliant Universally Unique
  Identifiers (UUIDs). It supports variant 1 UUIDs of versions:
    1 (time and node),
    3 (name (namespace+data), MD5),
    4 (random),
    5 (name (namespace+data), SHA-1),
    6 (time and node with improved locality),
    7 (UNIX time, random data).
  Additional API bindings are provided for C++98 (deprecated) and Perl:5.
  Optional backward compatibility exists for the ISO-C DCE-1.1
  and Perl Data::UUID APIs.

  UUIDs are 128 bit numbers which are intended to have a high likelihood
  of uniqueness over space and time and are computationally difficult
  to guess. They are globally unique identifiers which can be locally
  generated without contacting a global registration authority. UUIDs
  are intended as unique identifiers for both mass tagging objects
  with an extremely short lifetime and to reliably identifying very
  persistent objects across a network.

  HOME AND DOCUMENTATION

  This is a thawed OSSP project; for git repository/bug tracker/mailing list see
    https://sr.ht/~nabijaczleweli/ossp

  The manual is available on-line and at
    https://srhtcdn.githack.com/~nabijaczleweli/ossp-uuid/blob/man/ossp-uuid.pdf

  Release tarballs are signed with nabijaczleweli@nabijaczleweli.xyz
    (pull with WKD, but 7D69 474E 8402 8C5C C0C4  4163 BCFD 0B01 8D26 58F1).
  аnd stored in git notes as-if via the example program provided at
    https://man.sr.ht/git.sr.ht/#signing-tags-tarballs
  and are thus available on the refs listing/tag page as .tar.gz{,.asc}:
    https://git.sr.ht/~nabijaczleweli/ossp-uuid/refs

  COMPATIBILITY

  Wants libmd-dev, a C11 compiler, and autoconf/automake/libtool-bin.
  $ autoreconf -fi
  $ ./configure [--with-dce] [--with-cxx] [--with-perl] [--with-perl-compat] \
                [--with-libname=libossp-uuid] [usual autoconf variables]...
  #              the default is libuuid
  $ make ...

  MAC address detection has been verified to work under
  Linux, the Hurd, {Free,Net,Open}BSD, the Macintosh, and the illumos gate.

  COPYRIGHT AND LICENSE

  Files from the original OSSP project bear the licence seen below;
  new files are provided under the 0BSD licence (cf. LICENSES/0BSD).

  Copyright (c) 2004-2008 Ralf S. Engelschall <rse@engelschall.com>
  Copyright (c) 2004-2008 The OSSP Project <http://www.ossp.org/>

  This file is part of OSSP uuid, a library for the generation
  of UUIDs which can found at http://www.ossp.org/pkg/lib/uuid/

  Permission to use, copy, modify, and distribute this software for
  any purpose with or without fee is hereby granted, provided that
  the above copyright notice and this permission notice appear in all
  copies.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

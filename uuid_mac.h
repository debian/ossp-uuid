// SPDX-License-Identifier: 0BSD


#ifndef __UUID_MAC_H__
#define __UUID_MAC_H__

#include <stdbool.h>
#include <stddef.h>


#define MAC_LEN 6

extern __attribute__((visibility("hidden"))) bool mac_address(uint8_t * mac);


#endif

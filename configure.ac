dnl ##
dnl ##  OSSP uuid - Universally Unique Identifier
dnl ##  Copyright (c) 2004-2008 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2004-2008 The OSSP Project <http://www.ossp.org/>
dnl ##
dnl ##  This file is part of OSSP uuid, a library for the generation
dnl ##  of UUIDs which can found at http://www.ossp.org/pkg/lib/uuid/
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  configure.ac: GNU Autoconf source script
dnl ##

AC_PREREQ([2.71])
AC_INIT
UUID_VERSION_RAW="1.6.4"
UUID_VERSION_MDOC_DATE="December  9, 2024"
UUID_VERSION_STR="$UUID_VERSION_RAW (2024-12-09)"
UUID_VERSION_HEX=0x106204
UUID_VERSION_LIBTOOL=16:22
echo "ossp-uuid $UUID_VERSION_STR"
AC_SUBST(UUID_VERSION_RAW)
AC_SUBST(UUID_VERSION_MDOC_DATE)
AC_SUBST(UUID_VERSION_STR)
AC_SUBST(UUID_VERSION_HEX)
AC_SUBST(UUID_VERSION_LIBTOOL)

AC_PROG_MAKE_SET
AC_PROG_CC

LT_INIT


AC_ARG_WITH([libname], AS_HELP_STRING([--with-libname=libuuid], [library name (default libuuid), also affects DCE compat and C++ libraries]))
case "$with_libname" in
    yes|no|"") with_libname="libuuid" ;;
esac
libname="${with_libname#lib}"
AC_SUBST(libname)


AC_SEARCH_LIBS(getifaddrs, socket)

CFLAGS="$CFLAGS $(${PKG_CONFIG-"pkg-config"} --cflags libmd)"
LIBS="  $LIBS   $(${PKG_CONFIG-"pkg-config"} --libs   libmd)" || {
    AC_CHECK_FUNCS(SHA1Final)
    test "$ac_cv_func_SHA1Final" = yes || LIBS="$LIBS -lmd"
}


AC_CHECK_HEADERS(sys/param.h sys/time.h sys/socket.h sys/sockio.h sys/ioctl.h)
AC_CHECK_HEADERS(ifaddrs.h net/if.h net/if_dl.h net/if_arp.h netinet/in.h,,,
[[
#if HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
]])

AC_CHECK_FUNCS(getentropy)


AC_ARG_WITH([dce], AS_HELP_STRING([--with-dce], [build DCE 1.1 backward compatibility API]))
if test "$with_dce" = "yes"; then
    AC_SUBST(WITH_DCE, [dce])
else
    AC_SUBST(WITH_DCE, []):;
fi

AC_ARG_WITH([cxx], AS_HELP_STRING([--with-cxx], [build C++ bindings to C API]))
if test "$with_cxx" = "yes"; then
    AC_SUBST(WITH_CXX, [cxx])
    AC_PROG_CXX
else
    AC_SUBST(WITH_CXX, []):;
fi

AC_ARG_WITH([perl], AS_HELP_STRING([--with-perl], [build Perl bindings to C API]))
if test "$with_perl" = "yes"; then
    AC_SUBST(WITH_PERL, [perl])

    AC_ARG_WITH([perl-compat], AS_HELP_STRING([--with-perl-compat], [build Perl compatibility API]))
    ! test "$with_perl_compat" = "yes"; WITH_PERL_COMPAT=$?
    AC_SUBST(WITH_PERL_COMPAT)

    AC_PATH_PROG(PERL, perl, NA)
    if test "$PERL" = "NA"; then
        AC_MSG_ERROR(required Perl interpreter not found in \$PATH)
    fi
else
    AC_SUBST(WITH_PERL, []):;
fi

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES([Makefile uuid-config uuid.pc uuid.h uuid.1 uuid++.3 uuid-config.1 uuid.3])
AC_CONFIG_COMMANDS([substituted-executables], [chmod a+x uuid-config])
AC_OUTPUT

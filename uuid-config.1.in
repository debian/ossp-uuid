.\"
.\"  OSSP uuid - Universally Unique Identifier
.\"  Copyright (c) 2004-2008 Ralf S. Engelschall <rse@engelschall.com>
.\"  Copyright (c) 2004-2008 The OSSP Project <http://www.ossp.org/>
.\"
.\"  This file is part of OSSP uuid, a library for the generation
.\"  of UUIDs which can found at http://www.ossp.org/pkg/lib/uuid/
.\"
.\"  Permission to use, copy, modify, and distribute this software for
.\"  any purpose with or without fee is hereby granted, provided that
.\"  the above copyright notice and this permission notice appear in all
.\"  copies.
.\"
.\"  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
.\"  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
.\"  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\"  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
.\"  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
.\"  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
.\"  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
.\"  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
.\"  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\"  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\"  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\"  SUCH DAMAGE.
.\"
.\"  uuid-config.pod: library build utility manpage
.Dd @UUID_VERSION_MDOC_DATE@
.ds doc-volume-operating-system OSSP
.Dt UUID-CONFIG 1
.Os ossp-uuid @UUID_VERSION_RAW@
.
.Sh NAME
.Nm uuid-config
.Nd OSSP uuid API build utility
.Sh SYNOPSIS
.Nm
.Op Fl -help
.Op Fl -version
.Op Fl -prefix
.Op Fl -exec-prefix
.Op Fl -bindir
.Op Fl -libdir
.Op Fl -includedir
.Op Fl -mandir
.Op Fl -datadir
.Op Fl -acdir
.Op Fl -cflags
.Op Fl -ldflags
.Op Fl -libs
.
.Sh DESCRIPTION
The
.Nm
program is a little helper utility for easy configuring and
building of applications based on the
.Xr uuid 3
library.
It can be used to query the
C compiler and linker flags which are required to correctly compile and link
the application against the
.Xr uuid 3
library.
.Pp
OSSP uuid also ships
.Xr pkg-config 1
definition
.Pq as Pa @libname@.pc .
Use that instead.
.
.Sh OPTIONS
.Bl -tag -compact -width ".Fl -exec-prefix"
.It Fl -help
Prints the short usage information.
.
.It Fl -version
Prints the version number and date of the installed
.Xr uuid 3
library.
.
.It Fl -prefix
Prints the installation prefix of architecture independent files
.
.It Fl -exec-prefix
Prints the installation prefix of architecture dependent files.
.
.It Fl -bindir
Prints the installation directory of binaries.
.
.It Fl -libdir
Prints the installation directory of libraries.
.
.It Fl -includedir
Prints the installation directory of include headers.
.
.It Fl -mandir
Prints the installation directory of manual pages.
.
.It Fl -datadir
Prints the installation directory of shared data.
.
.It Fl -acdir
Prints the installation directory of
.Nm autoconf
data.
.
.It Fl -cflags
Prints the C compiler flags which are needed to compile the
.Xr uuid 3 Ns -based
application.
The output is usually added to the
.Ev CFLAGS
uuidiable of the
applications
.Pa Makefile .
.
.It Fl ldflags
Prints the linker flags
.Pq Fl L
which are needed to link the application with
the
.Xr uuid 3
library.
The output is usually added to the
.Ev LDFLAGS
uuidiable of
the applications
.Pa Makefile .
.
.It Fl libs
Prints the library flags
.Pq Fl l
which are needed to link the application with
the C
.Xr uuid 3
library.
The output is usually added to the
.Ev LIBS
uuidiable of the
applications
.Pa Makefile .
.El
.
.Sh EXAMPLES
.Bd -literal -compact
CC      = cc
CFLAGS  = -O `uuid-config --cflags`
LDFLAGS = `uuid-config --ldflags`
LIBS    = -lm `uuid-config --libs`

all: foo
foo: foo.o
	$(CC) $(LDFLAGS) -o foo foo.o $(LIBS)
foo.o: foo.c
	$(CC) $(CFLAGS) -c foo.c
.Ed
.
.Sh SEE ALSO
.Xr uuid 1 ,
.Xr uuid 3 ,
.Xr OSSP::uuid 3

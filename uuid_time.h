// SPDX-License-Identifier: 0BSD


#ifndef __UUID_TIME_H__
#define __UUID_TIME_H__

#if __has_include(<sched.h>)
#include <sched.h>
#endif


#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
static int sched_yield() {
	SwitchToThread();
	return 0;
}
#endif


#endif
